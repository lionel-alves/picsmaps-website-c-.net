﻿using System.Collections.Generic;
using System.Linq;
using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace PicsMapWeb.Models
{
    public class SearchModel
    {
        [Required]
        [Display(Name = "Login")]
        public string Login { get; set; }
    }
}
