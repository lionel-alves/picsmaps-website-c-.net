﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace PicsMapWeb.Models
{

    public class PhotoModel
    {
        [Required]
        [StringLength(50, ErrorMessage = "Le {0} ne doit pas dépasser {1} caractères.")]
        [Display(Name = "Titre")]
        public string Title { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Le {0} ne doit pas dépasser {1} caractères.")]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Tags (Si plusieurs tags, séparer par des virgules sans espaces. Exemple: 'nature,animal,chien')")]
        public string Tags { get; set; }

        [Display(Name = "latitude")]
        public string latitude { get; set; }

        [Display(Name = "longitude")]
        public string longitude { get; set; }
    }
}
