﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace PicsMapWeb.Models {
    public class MyMembershipUser : MembershipUser {
        private DataAccess.Membership user;

        public DataAccess.Membership User {
            get { return user; }
            set { user = value; }
        }
        
    }
}