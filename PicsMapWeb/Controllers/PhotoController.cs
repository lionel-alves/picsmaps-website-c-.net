﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PicsMapWeb.Models;
using System.Web.Helpers;
using System.IO;

namespace PicsMapWeb.Controllers
{
    public class PhotoController : Controller
    {
        public class ShowPhotoModel
        {
            public int idPhoto { get; set; }
            public int idCpt { get; set; }
        }
        //
        // GET: /Photo/
        public ViewResult Show(ShowPhotoModel model)
        {
            int idPhoto = model.idPhoto;
            int idCpt = model.idCpt;

            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);
            
            ViewBag.cptName = cpt.Nom;
            ViewBag.cptFirstname = cpt.Prenom;
            ViewBag.cptLogin = cpt.Login;
            ViewBag.cptEmail = cpt.Email;

            DataAccess.Photo currentPhoto = BusinessManagement.Photo.findById(idPhoto);
            ViewBag.currentPhoto = currentPhoto;

            DataAccess.Membership cptCurrentPhotoAuthor = BusinessManagement.Compte.findById(idCpt);
            ViewBag.currentPhotoAuthor = cptCurrentPhotoAuthor;

            List<DataAccess.Photo> currentAuthorListPhotos = new List<DataAccess.Photo>();
            currentAuthorListPhotos = BusinessManagement.Photo.findForAccount(0, 10, cptCurrentPhotoAuthor.Login);
            ViewBag.photosList = currentAuthorListPhotos;            
            /*
            List<DataAccess.Photo> myListPhotos = new List<DataAccess.Photo>();
            myListPhotos = BusinessManagement.Photo.findForAccount(0,10, cpt.Login);
            ViewBag.photosList = myListPhotos;

            List<DataAccess.Photo> listFriendsPhotos = new List<DataAccess.Photo>();
            listFriendsPhotos = BusinessManagement.Photo.findAllFriendPictures(0, 10, cpt.Login);          
            ViewBag.friendPhotosList = listFriendsPhotos;

            List<DataAccess.Photo> listRecentFriendsPhotos = new List<DataAccess.Photo>();
            listRecentFriendsPhotos = BusinessManagement.Photo.findRecentFriendPhoto(0, 10, cpt.Login, DateTime.Now.AddDays(-3));
            ViewBag.friendsRecentPhotosList = listRecentFriendsPhotos;

            List<DataAccess.Photo> listRecentPhotos = new List<DataAccess.Photo>();
            listRecentPhotos = BusinessManagement.Photo.findLastPhoto(0, 10, DateTime.Now.AddDays(-3));
            ViewBag.recentPhotosList = listRecentPhotos;
            */

            // attention offset et index inversés !
            List<DataAccess.Commentaire> listLastComments = new List<DataAccess.Commentaire>();
            listLastComments = BusinessManagement.Commentaire.findComments(30, 0, idPhoto);
            
            List<CommentListModel> lComment = new List<CommentListModel>();
            foreach (DataAccess.Commentaire comm in listLastComments) {
                lComment.Add(new CommentListModel() {
                    Id = comm.Id,
                    authFirstname = comm.Membership.Prenom,
                    authName = comm.Membership.Nom,
                    date = comm.Date.Value,
                    Texte = comm.Texte,
                    IdCpt = comm.cpt_Id,
                    PhotoUserProfil = comm.Membership.Photo_Profil
                });
            }
            ViewBag.lastCommentsList = lComment;

            // Modifier l'id de la photo, par défaut = 1
            double noteMoyenne = BusinessManagement.Evaluation.findEvalsForPicture(idPhoto);
            if (Double.IsNaN(noteMoyenne))
                noteMoyenne = 0;
            ViewBag.noteMoyennePhoto = noteMoyenne;
            ViewBag.noteClasses = String.Format("note note_{0}", noteMoyenne);

            ViewBag.isEvaluated = BusinessManagement.Evaluation.hasEvaluate(idPhoto, cpt.Login);
            //deleteEval(long photoId, string login)
            //findCommentsForPicture(long photoId)

            List<DataAccess.Photo> PhotosCopy = new List<DataAccess.Photo>();


            DataAccess.Photo p = new DataAccess.Photo();
            p.Titre = currentPhoto.Titre;
            p.longitude = currentPhoto.longitude;
            p.latitude = currentPhoto.latitude;
            p.Url = currentPhoto.Url;
            PhotosCopy.Add(p);


            System.Web.Script.Serialization.JavaScriptSerializer oSerializer =
            new System.Web.Script.Serialization.JavaScriptSerializer();
            string sJSON = oSerializer.Serialize(PhotosCopy);
            ViewBag.markerListJS = sJSON;

            return View();
        }

        [HttpPost]
        public ActionResult Show(CommentModel model)
        {
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);

            if (ModelState.IsValid)
            {
                try
                {
                    BusinessManagement.Commentaire.createComment(model.IdPhoto, model.Texte, cpt.Login);

                    return Show(new ShowPhotoModel() { idPhoto = model.IdPhoto, idCpt = model.IdAuthor });
                }
                catch (BusinessManagement.BusinessManagementException e)
                {
                    ModelState.AddModelError("", e.Message);
                }
            }

            // If we got this far, something failed, redisplay form
            return Show(new ShowPhotoModel() { idPhoto = model.IdPhoto, idCpt = model.IdAuthor });
        }

        public ActionResult DoEval1Star(int idPhoto)
        {
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);

            ViewBag.cptName = cpt.Nom;
            ViewBag.cptFirstname = cpt.Prenom;
            ViewBag.cptLogin = cpt.Login;
            ViewBag.cptEmail = cpt.Email;

            BusinessManagement.Evaluation.createEval(idPhoto, cpt.Login, 1);

            return RedirectToAction("Show", new { idPhoto = idPhoto, idCpt = cpt.Id });
            
        }

        public ActionResult DoEval2Star(int idPhoto)
        {
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);

            ViewBag.cptName = cpt.Nom;
            ViewBag.cptFirstname = cpt.Prenom;
            ViewBag.cptLogin = cpt.Login;
            ViewBag.cptEmail = cpt.Email;

            BusinessManagement.Evaluation.createEval(idPhoto, cpt.Login, 2);

            return RedirectToAction("Show", new { idPhoto = idPhoto, idCpt = cpt.Id });
        }

        public ActionResult DoEval3Star(int idPhoto)
        {
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);

            ViewBag.cptName = cpt.Nom;
            ViewBag.cptFirstname = cpt.Prenom;
            ViewBag.cptLogin = cpt.Login;
            ViewBag.cptEmail = cpt.Email;

            BusinessManagement.Evaluation.createEval(idPhoto, cpt.Login, 3);

            return RedirectToAction("Show", new { idPhoto = idPhoto, idCpt = cpt.Id });
        }

        public ActionResult DoEval4Star(int idPhoto)
        {
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);

            ViewBag.cptName = cpt.Nom;
            ViewBag.cptFirstname = cpt.Prenom;
            ViewBag.cptLogin = cpt.Login;
            ViewBag.cptEmail = cpt.Email;

            BusinessManagement.Evaluation.createEval(idPhoto, cpt.Login, 4);

            return RedirectToAction("Show", new { idPhoto = idPhoto, idCpt = cpt.Id });
        }

        public ActionResult DoEval5Star(int idPhoto)
        {
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);

            ViewBag.cptName = cpt.Nom;
            ViewBag.cptFirstname = cpt.Prenom;
            ViewBag.cptLogin = cpt.Login;
            ViewBag.cptEmail = cpt.Email;

            BusinessManagement.Evaluation.createEval(idPhoto, cpt.Login, 5);

            return RedirectToAction("Show", new { idPhoto = idPhoto, idCpt = cpt.Id });
        }
        public ActionResult DeleteEval(int idPhoto)
        {
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);

            ViewBag.cptName = cpt.Nom;
            ViewBag.cptFirstname = cpt.Prenom;
            ViewBag.cptLogin = cpt.Login;
            ViewBag.cptEmail = cpt.Email;

            // Modifier l'id de la photo, par défaut = 1
            BusinessManagement.Evaluation.deleteEval(idPhoto, cpt.Login);

            return RedirectToAction("Show", new { idPhoto = idPhoto, idCpt = cpt.Id });
        }

        public ViewResult DeleteComment(int commentId)
        {
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);

            ViewBag.cptName = cpt.Nom;
            ViewBag.cptFirstname = cpt.Prenom;
            ViewBag.cptLogin = cpt.Login;
            ViewBag.cptEmail = cpt.Email;

            BusinessManagement.Commentaire.deleteComment(commentId);

            return View("Show");

        }

        
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(PhotoModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    DataAccess.Photo photo = new DataAccess.Photo();
                    photo.Titre = model.Title;
                    photo.Description = model.Description;
                    photo.date_Poste = DateTime.Now;
                    photo.accept = true;
                    photo.latitude = double.Parse(model.latitude, System.Globalization.CultureInfo.CreateSpecificCulture("en"));
                    photo.longitude = double.Parse(model.longitude, System.Globalization.CultureInfo.CreateSpecificCulture("en"));

                    // Gestion de l'image
                    var webHelper = new Helpers.WebImageHelper();
                    var image = webHelper.GetImageFromRequest();
                    if (image != null)
                    {
                        if (image.Width > 500)
                            image.Resize(500, ((500 * image.Height) / image.Width));

                        // Generate file name
                        var filename = Path.GetFileName(image.FileName);
                        Random rand = new Random();
                        filename = String.Format("{0}_{1}_{2}_{3}_{4}", BusinessManagement.Compte.findByLogin(User.Identity.Name).Id,
                            DateTime.Now.Second + DateTime.Now.Minute + DateTime.Now.Hour,
                            DateTime.Now.Day + DateTime.Now.Month + DateTime.Now.Year, rand.Next(50000), filename);
                        filename = Path.Combine("~/Content/upload", filename);

                        // Save file
                        image.Save(filename);

                        photo.Url = Url.Content(filename);
                    }
                    else
                        throw new BusinessManagement.BusinessManagementException("La photo est incorrect");


                    // Gestion des tags
                    List<string> tagsList = new List<string>();
                    if (model.Tags != null)
                    {
                        model.Tags = model.Tags.ToLower();
                        string[] tags = model.Tags.Split(',');
                        tagsList.AddRange(tags);
                    }

                    BusinessManagement.Photo.createPhoto(photo, User.Identity.Name, tagsList);
                    return RedirectToAction("Private", "Home");
                }
                catch (BusinessManagement.BusinessManagementException e)
                {
                    ModelState.AddModelError("", e.Message);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
    }
}
