﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PicsMapWeb.Models;
using System.Web.Security;

namespace PicsMapWeb.Controllers
{
    public class PhotoSerialized
    {
        public string Titre { get; set; }

        public string Description { get; set; }

        public double? longitude { get; set; }

        public double? latitude { get; set; }

        public string Url { get; set; }

        public string date_Poste { get; set; }

        public string login { get; set; }
    }

    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(model.UserName, model.Password))
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                    var cpt = BusinessManagement.Compte.findByLogin(model.UserName);
                    Session.Add("CurrentUserId", cpt.Id);
                    Session.Add("CurrentUserPhotoUrl", cpt.Photo_Profil);

                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                        && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Private", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "L'email ou le mot de passe est incorrect.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult Private(string index, string tag)
        {
            ViewBag.Message = "Page accueil d'un user connecté : avec map et liste des ses dernières photos et celles de ses amis";
            
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);
            Session.Add("CurrentUserPhotoUrl", cpt.Photo_Profil);

            ViewBag.cptName = cpt.Nom;
            ViewBag.cptFirstname = cpt.Prenom;
            ViewBag.cptLogin = cpt.Login;
            ViewBag.cptEmail = cpt.Email;

            if (index == "4")
            {
                List<DataAccess.Photo> myListPhotos = new List<DataAccess.Photo>();
                myListPhotos = BusinessManagement.Photo.findForAccount(0, 14, cpt.Login);
                ViewBag.photosList = myListPhotos;
            }
            else if (index == "3")
            {
                List<DataAccess.Photo> listFriendsPhotos = new List<DataAccess.Photo>();
                listFriendsPhotos = BusinessManagement.Photo.findAllFriendPictures(0, 14, cpt.Login);
                ViewBag.photosList = listFriendsPhotos;
            }
            else if (index == "2")
            {
                List<DataAccess.Photo> listRecentFriendsPhotos = new List<DataAccess.Photo>();
                listRecentFriendsPhotos = BusinessManagement.Photo.findRecentFriendPhoto(0, 14, cpt.Login, DateTime.Now.AddDays(-3));
                ViewBag.photosList = listRecentFriendsPhotos;
            }
            else
            {
                List<DataAccess.Photo> listRecentPhotos = new List<DataAccess.Photo>();
                listRecentPhotos = BusinessManagement.Photo.findLastPhoto(0, 14);
                ViewBag.photosList = listRecentPhotos;
            }



            if (tag != null && tag != "")
            {
                ViewBag.photosList = BusinessManagement.Photo.filterByTag(ViewBag.photosList, tag);
            }

            Dictionary<string, PhotoSerialized> PhotosCopy = new Dictionary<string, PhotoSerialized>();
            List<PhotoSerialized> MarkersCopy = new List<PhotoSerialized>();
            foreach (DataAccess.Photo photo in ViewBag.photosList)
            {
                DataAccess.Membership owner = BusinessManagement.Compte.findById((int)photo.cpt_Id);
                PhotoSerialized p = new PhotoSerialized();
              p.Titre = photo.Titre;
              p.longitude = photo.longitude;
              p.latitude = photo.latitude;
              p.Url = photo.Url;
              p.Description = photo.Description;
              p.date_Poste = ((DateTime)photo.date_Poste).ToString("MM/dd/yyyy");
              p.login = owner.Login;
              MarkersCopy.Add(p);
              PhotosCopy.Add(photo.Id.ToString(), p);
            }
            System.Web.Script.Serialization.JavaScriptSerializer oSerializer =
            new System.Web.Script.Serialization.JavaScriptSerializer();

            ViewBag.photoListJS = oSerializer.Serialize(PhotosCopy);

            ViewBag.markerListJS = oSerializer.Serialize(MarkersCopy);
            return View(new FilterModel());
        }

        [HttpPost]
        public ActionResult Private(FilterModel model)
        {
            return Private(model.Filter, model.tagValue);
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
