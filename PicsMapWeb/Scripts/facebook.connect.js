﻿
window.fbAsyncInit = function () {
    FB.init({ appId: '167440046722286', 
                status: true, cookie: false, xfbml: true });
};
function afterFacebookConnect() {
    FB.getLoginStatus(function (response) {
        if (response.session) {
            window.location = "../Account/FacebookLogin?token=" + 
                    response.session.access_token;
        } else {
            // user clicked Cancel
        }
    });
};
$(document).ready(function () {
    if (document.getElementById('fb-root') != undefined) {
        var e = document.createElement('script');
        e.type = 'text/javascript';
        e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
        e.async = true;
        document.getElementById('fb-root').appendChild(e);
    }
});

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/fr_FR/all.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
} (document, 'script', 'facebook-jssdk'));