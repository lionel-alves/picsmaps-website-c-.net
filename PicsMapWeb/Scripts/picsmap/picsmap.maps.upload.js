
var geocoder = new google.maps.Geocoder();
var marker = null;

function geocodePosition(pos) {
  geocoder.geocode({
	latLng: pos
  }, function(responses) {
	if (responses && responses.length > 0) {
	  updateMarkerAddress(responses[0].formatted_address);
	} else {
	  updateMarkerAddress('Cannot determine address at this location.');
	}
  });
}

function updateMarkerStatus(str) {
  document.getElementById('markerStatus').innerHTML = str;
}

function updateMarkerPosition(latLng) {
  document.getElementById('info').innerHTML = [
	latLng.lat(),
	latLng.lng()
  ].join(', ');
  document.getElementById('latitude').setAttribute("value", latLng.lat());
  document.getElementById('longitude').setAttribute("value", latLng.lng());
}

function updateMarkerAddress(str) {
  document.getElementById('address').innerHTML = str;
}

/**
 * Initializes the map and listeners.
 */
function initialize() {
    var latLng = new google.maps.LatLng(48.815422800206086, 2.362988418579107);
    document.getElementById('latitude').setAttribute("value", latLng.lat());
    document.getElementById('longitude').setAttribute("value", latLng.lng());
  var map = new google.maps.Map(document.getElementById('mapCanvas'), {
	zoom: 8,
	center: latLng,
	mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  marker = new google.maps.Marker({
	position: latLng,
	title: 'Point A',
	map: map,
	draggable: true
  });
  
  // Update current position info.
  updateMarkerPosition(latLng);
  geocodePosition(latLng);
  
  // Add dragging event listeners.
  google.maps.event.addListener(marker, 'dragstart', function() {
	updateMarkerAddress('Dragging...');
  });
  
  google.maps.event.addListener(marker, 'drag', function() {
	updateMarkerStatus('Dragging...');
	updateMarkerPosition(marker.getPosition());
  });
  
  google.maps.event.addListener(marker, 'dragend', function() {
	updateMarkerStatus('Drag ended');
	geocodePosition(marker.getPosition());
  });
  
  google.maps.event.addListener(map, 'click', function(event) {
	marker.setMap(null);
	marker = new google.maps.Marker({
		position: event.latLng,
		map: map
	});
	geocodePosition(marker.getPosition());
	updateMarkerPosition(marker.getPosition());
  });
}

google.maps.event.addDomListener(window, 'load', initialize);

function clickclear(thisfield, defaulttext) {
    if (thisfield.value == defaulttext) {
        thisfield.value = "";
    }
}

function clickclearpassword(thisfield, defaulttext) {
    if (thisfield.value == defaulttext) {
        thisfield.value = "";
        thisfield.type = "password";
    }
}