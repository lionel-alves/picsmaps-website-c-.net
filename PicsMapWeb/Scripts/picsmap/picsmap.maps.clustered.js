
var mc = null;
var clusterMap = null;

var markerImages = null;

function toggleMarkerClusterer() {
    mc = new MarkerClusterer(clusterMap, markers, { maxZoom: 19 });
}

/**
 * Info Window.
 */
var infoWindow = new google.maps.InfoWindow;

function openInfoWindow() {
  var marker = this;

  infoWindow.setContent('<div class="pushpin_content"><p>' + marker.getTitle() + '</p><img src="' + markerImages[marker.title] + '" alt="" height="100" width="auto" /></div>');

  infoWindow.open(clusterMap, marker);
};
	
/**
 * Initializes the clusterMap and listeners.
 */
function initializeClusterMap() {
    clusterMap = new google.maps.Map(document.getElementById('map'), {
	center: new google.maps.LatLng(38, 15),
	zoom: 2,
	mapTypeId: 'terrain'
  });


  // info Window
	  google.maps.event.addListener(map, 'click', function() {
	  infoWindow.close();
	});

    // cluster the markers
    toggleMarkerClusterer();
}

function updateClusterMap() {
    markerImages = [];

    // Prepares the marker object, creating a google.maps.Marker object for each
    if (markers) {
        for (var i = 0; i < markers.length; i++) {
            var details = markers[i];
            var marker = new google.maps.Marker({
                title: details.Titre,
                position: new google.maps.LatLng(
			    details.latitude, details.longitude),
                clickable: true,
                draggable: false,
                flat: true
            });
            markerImages[marker.title] = details.Url;
            google.maps.event.addListener(marker, 'click', openInfoWindow);
            markers[i] = marker;
        }
    }
}
  
google.maps.event.addDomListener(window, 'load', initializeClusterMap);
