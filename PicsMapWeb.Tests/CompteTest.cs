﻿using BusinessManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PicsMapWeb.Tests
{
    
    
    /// <summary>
    ///Classe de test pour CompteTest, destinée à contenir tous
    ///les tests unitaires CompteTest
    ///</summary>
    [TestClass()]
    public class CompteTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Obtient ou définit le contexte de test qui fournit
        ///des informations sur la série de tests active ainsi que ses fonctionnalités.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Attributs de tests supplémentaires
        // 
        //Vous pouvez utiliser les attributs supplémentaires suivants lorsque vous écrivez vos tests :
        //
        //Utilisez ClassInitialize pour exécuter du code avant d'exécuter le premier test dans la classe
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Utilisez ClassCleanup pour exécuter du code après que tous les tests ont été exécutés dans une classe
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Utilisez TestInitialize pour exécuter du code avant d'exécuter chaque test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Utilisez TestCleanup pour exécuter du code après que chaque test a été exécuté
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///Test pour Constructeur Compte
        ///</summary>
        [TestMethod()]
        public void CompteConstructorTest()
        {
            BusinessManagement.Compte target = new BusinessManagement.Compte();
            Assert.IsNotNull(target);
        }

        /// <summary>
        ///Test pour UpdateCompte
        ///</summary>
        [TestMethod()]
        public void UpdateCompteTest()
        {
            DataAccess.Membership cpt = null; // TODO: initialisez à une valeur appropriée
            BusinessManagement.Compte.UpdateCompte(cpt, cpt.Login);
            Assert.Inconclusive("Une méthode qui ne retourne pas une valeur ne peut pas être vérifiée.");
        }


        /// <summary>
        ///Test pour creerCompte
        ///</summary>
        [TestMethod()]
        public void creerCompteTest()
        {
            DataAccess.Membership cpt = null; // TODO: initialisez à une valeur appropriée
            BusinessManagement.Compte.creerCompte(cpt);
            Assert.Inconclusive("Une méthode qui ne retourne pas une valeur ne peut pas être vérifiée.");
        }

        /// <summary>
        ///Test pour creerCompte
        ///</summary>
        [TestMethod()]
        public void creerCompteTest1()
        {
            string login = "shik4"; // TODO: initialisez à une valeur appropriée
            string email = "shik4@gmail.com"; // TODO: initialisez à une valeur appropriée
            string nom = "shik4"; // TODO: initialisez à une valeur appropriée
            string prenom = "shik4"; // TODO: initialisez à une valeur appropriée
            string mdp = "shik4"; // TODO: initialisez à une valeur appropriée
            BusinessManagement.Compte.creerCompte(login, email, nom, prenom, mdp, "/Content/i/photo.jpg");

            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin("shik4");
            Assert.IsNotNull(cpt);
        }

        /// <summary>
        ///Test pour deleteCompte
        ///</summary>
        [TestMethod()]
        public void deleteCompteTest()
        {
            string login = string.Empty; // TODO: initialisez à une valeur appropriée
            BusinessManagement.Compte.deleteCompte(login);
            Assert.Inconclusive("Une méthode qui ne retourne pas une valeur ne peut pas être vérifiée.");
        }

        /// <summary>
        ///Test pour findByCriteria
        ///</summary>
        [TestMethod()]
        public void findByCriteriaTest()
        {
            int index = 0; // TODO: initialisez à une valeur appropriée
            int offset = 0; // TODO: initialisez à une valeur appropriée
            DataAccess.Membership cpt = null; // TODO: initialisez à une valeur appropriée
            System.Collections.Generic.List<DataAccess.Membership> expected = null; // TODO: initialisez à une valeur appropriée
            System.Collections.Generic.List<DataAccess.Membership> actual;
            actual = BusinessManagement.Compte.findByCriteria(index, offset, cpt);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Vérifiez l\'exactitude de cette méthode de test.");
        }

        /// <summary>
        ///Test pour findByCriteria
        ///</summary>
        [TestMethod()]
        public void findByCriteriaTest1()
        {
            int index = 0; // TODO: initialisez à une valeur appropriée
            int offset = 0; // TODO: initialisez à une valeur appropriée
            string login = string.Empty; // TODO: initialisez à une valeur appropriée
            string email = string.Empty; // TODO: initialisez à une valeur appropriée
            string nom = string.Empty; // TODO: initialisez à une valeur appropriée
            string prenom = string.Empty; // TODO: initialisez à une valeur appropriée
            System.Collections.Generic.List<DataAccess.Membership> expected = null; // TODO: initialisez à une valeur appropriée
            System.Collections.Generic.List<DataAccess.Membership> actual;
            actual = BusinessManagement.Compte.findByCriteria(index, offset, login, email, nom, prenom);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Vérifiez l\'exactitude de cette méthode de test.");
        }

        /// <summary>
        ///Test pour findByLogin
        ///</summary>
        [TestMethod()]
        public void findByLoginTest()
        {
            string login = "terry"; // TODO: initialisez à une valeur appropriée
            var actual = BusinessManagement.Compte.findByLogin(login);
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///Test pour followCompte
        ///</summary>
        [TestMethod()]
        public void followCompteTest()
        {
            string loginOrigin = string.Empty; // TODO: initialisez à une valeur appropriée
            string loginSuivi = string.Empty; // TODO: initialisez à une valeur appropriée
            BusinessManagement.Compte.followCompte(loginOrigin, loginSuivi);
            Assert.Inconclusive("Une méthode qui ne retourne pas une valeur ne peut pas être vérifiée.");
        }
    }
}
