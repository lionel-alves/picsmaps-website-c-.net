﻿using BusinessManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PicsMapWeb.Tests
{
    
    
    /// <summary>
    ///Classe de test pour PhotoTest, destinée à contenir tous
    ///les tests unitaires PhotoTest
    ///</summary>
    [TestClass()]
    public class PhotoTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Obtient ou définit le contexte de test qui fournit
        ///des informations sur la série de tests active ainsi que ses fonctionnalités.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Attributs de tests supplémentaires
        // 
        //Vous pouvez utiliser les attributs supplémentaires suivants lorsque vous écrivez vos tests :
        //
        //Utilisez ClassInitialize pour exécuter du code avant d'exécuter le premier test dans la classe
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Utilisez ClassCleanup pour exécuter du code après que tous les tests ont été exécutés dans une classe
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Utilisez TestInitialize pour exécuter du code avant d'exécuter chaque test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Utilisez TestCleanup pour exécuter du code après que chaque test a été exécuté
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///Test pour Constructeur Photo
        ///</summary>
        [TestMethod()]
        public void PhotoConstructorTest()
        {
            BusinessManagement.Photo target = new BusinessManagement.Photo();
            Assert.Inconclusive("TODO: implémentez le code pour vérifier la cible");
        }

        /// <summary>
        ///Test pour createPhoto
        ///</summary>
        [TestMethod()]
        public void createPhotoTest()
        {
            DataAccess.Photo photoToCreate = null; // TODO: initialisez à une valeur appropriée
            string accountLogin = string.Empty; // TODO: initialisez à une valeur appropriée
            System.Collections.Generic.List<string> tagList = null; // TODO: initialisez à une valeur appropriée
            BusinessManagement.Photo.createPhoto(photoToCreate, accountLogin, tagList);
            Assert.Inconclusive("Une méthode qui ne retourne pas une valeur ne peut pas être vérifiée.");
        }

        /// <summary>
        ///Test pour findAllFriendPictures
        ///</summary>
        [TestMethod()]
        public void findAllFriendPicturesTest()
        {
            int index = 0; // TODO: initialisez à une valeur appropriée
            int offset = 0; // TODO: initialisez à une valeur appropriée
            string login = string.Empty; // TODO: initialisez à une valeur appropriée
            System.Collections.Generic.List<DataAccess.Photo> expected = null; // TODO: initialisez à une valeur appropriée
            System.Collections.Generic.List<DataAccess.Photo> actual;
            actual = BusinessManagement.Photo.findAllFriendPictures(index, offset, login);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Vérifiez l\'exactitude de cette méthode de test.");
        }

        /// <summary>
        ///Test pour findForAccount
        ///</summary>
        [TestMethod()]
        public void findForAccountTest()
        {
            int index = 0; // TODO: initialisez à une valeur appropriée
            int offset = 0; // TODO: initialisez à une valeur appropriée
            string login = string.Empty; // TODO: initialisez à une valeur appropriée
            System.Collections.Generic.List<DataAccess.Photo> expected = null; // TODO: initialisez à une valeur appropriée
            System.Collections.Generic.List<DataAccess.Photo> actual;
            actual = BusinessManagement.Photo.findForAccount(index, offset, login);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Vérifiez l\'exactitude de cette méthode de test.");
        }

        /// <summary>
        ///Test pour findRecentFriendPhoto
        ///</summary>
        [TestMethod()]
        public void findRecentFriendPhotoTest()
        {
            int index = 0; // TODO: initialisez à une valeur appropriée
            int offset = 0; // TODO: initialisez à une valeur appropriée
            string login = string.Empty; // TODO: initialisez à une valeur appropriée
            System.DateTime dateLimit = new System.DateTime(); // TODO: initialisez à une valeur appropriée
            System.Collections.Generic.List<DataAccess.Photo> expected = null; // TODO: initialisez à une valeur appropriée
            System.Collections.Generic.List<DataAccess.Photo> actual;
            actual = BusinessManagement.Photo.findRecentFriendPhoto(index, offset, login, dateLimit);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Vérifiez l\'exactitude de cette méthode de test.");
        }

        /// <summary>
        ///Test pour updatePhoto
        ///</summary>
        [TestMethod()]
        public void updatePhotoTest()
        {
            int idPhotoToUpdate = 0; // TODO: initialisez à une valeur appropriée
            DataAccess.Photo updatedPhoto = null; // TODO: initialisez à une valeur appropriée
            BusinessManagement.Photo.updatePhoto(idPhotoToUpdate, updatedPhoto);
            Assert.Inconclusive("Une méthode qui ne retourne pas une valeur ne peut pas être vérifiée.");
        }
    }
}
