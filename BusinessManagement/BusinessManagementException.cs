﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessManagement {
    public class BusinessManagementException : Exception {

        public BusinessManagementException()
            : base() {
        }

        public BusinessManagementException(string message) : base(
            message) {
        }

        public BusinessManagementException(string message, Exception innerException)
            : base(message, innerException) {
        }
    }
}
