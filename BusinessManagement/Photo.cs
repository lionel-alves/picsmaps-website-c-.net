﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace BusinessManagement {
    public class Photo {


        public static List<DataAccess.Photo> findForAccount(int index, int offset, string login) {
            try {
                List<DataAccess.Photo> res = PhotoAcces.findForAccount(index, offset, login);
                return res == null ? new List<DataAccess.Photo>() : res;
            } catch (Exception e) {
                
                throw new BusinessManagementException("failed to load the pictures for the account " + login +
                    ". Maybe the account is unexistant", e);
            }
        }

        public static List<DataAccess.Photo> findAllFriendPictures(int index, int offset, string login) {
            try {
                List<DataAccess.Photo> res = PhotoAcces.findFriendPictureForAccount(index, offset, login);
                return res == null ? new List<DataAccess.Photo>() : res;
            } catch (Exception e) {
                throw new BusinessManagementException("failed to load the friend's pictures for the account " + login +
                    ". Maybe the account is unexistant", e);
            }
        }

        public static List<DataAccess.Photo> findRecentFriendPhoto(int index, int offset, string login, DateTime dateLimit) {
            try {
                List<DataAccess.Photo> res =  PhotoAcces.findFriendPictureForAccount(index, offset, login, dateLimit);
                return res == null ? new List<DataAccess.Photo>() : res;
            } catch (Exception e) {
                throw new BusinessManagementException("failed to load the friend's pictures for the account " + login +
                    ". Maybe the account is unexistant", e);
            }
        }

        public static void updatePhoto(int idPhotoToUpdate, DataAccess.Photo updatedPhoto) {
            try {
                DataAccess.Photo toUpdate = PhotoAcces.findById(idPhotoToUpdate);
                PhotoAcces.updatePhoto(toUpdate, updatedPhoto);
            } catch (Exception e) {
                
                throw new BusinessManagementException("failed to update photo", e);
            }
        }

        public static void createPhoto(DataAccess.Photo photoToCreate, string accountLogin, List<string> tagList) {
            try {
                // TODO: Create Tag
                List<DataAccess.Tag> tags = new List<DataAccess.Tag>();
                foreach (string tag in tagList) {
                    tags.Add(new Tag() {
                        Label = tag
                    });
                }
                PhotoAcces.createPhoto(accountLogin, tags, photoToCreate);
            } catch (Exception e) {    
                throw new BusinessManagementException("fail to create new picture", e);
            }
        }

        public static List<DataAccess.Photo> findLastPhoto(int index, int offset, DateTime dateLimit) {
            try {
                List<DataAccess.Photo> res =  PhotoAcces.findLastPhoto(index, offset, dateLimit);
                return res == null ? new List<DataAccess.Photo>() : res;
            } catch (Exception e) {
                 throw new BusinessManagementException("failed to get all photos", e);
            }
        }

        public static List<DataAccess.Photo> findLastPhoto(int index, int offset) {
            try {
                List<DataAccess.Photo> res = PhotoAcces.findLastPhoto(index, offset);
                return res == null ? new List<DataAccess.Photo>() : res;
            } catch (Exception e) {
                throw new BusinessManagementException("failed to get all photos", e);
            }
        }

        public static DataAccess.Photo findById(int id) {
            try {
                return DataAccess.PhotoAcces.findById(id);
            } catch (Exception e) {
                throw new BusinessManagementException("failed to get photo with id " + id, e);
            }
        }

        public static bool deleteTag(string tag, int idPhoto) {
            try {
                return PhotoAcces.deleteTag(tag, idPhoto);
            } catch (Exception e) {
                throw new BusinessManagementException("failed to get all photos", e);
            }

        }

        public static void addTag(string tag, int idPhoto) {
            try {
                PhotoAcces.addTag(tag, idPhoto);
            } catch (Exception e) {
                throw new BusinessManagementException("failed to get all photos", e);
            }
        }

        public static List<DataAccess.Photo> getByTag(int index, int offset, string tag) {
            try {
                List<DataAccess.Photo> res = PhotoAcces.getByTag(index, offset, tag);
                return res == null ? new List<DataAccess.Photo>() : res;
            } catch (Exception e) {
                throw new BusinessManagementException("failed to get all photos", e);
            }
        }

        public static List<DataAccess.Photo> filterByTag(List<DataAccess.Photo> photos, string tag) {
            List<DataAccess.Photo> res = new List<DataAccess.Photo>();
            
            foreach (DataAccess.Photo photo in photos) {
                if (!photo.Tags.IsLoaded) {
                    photo.Tags.Load();
                }
                foreach (DataAccess.Tag tagPht in photo.Tags) {
                    if (tagPht.Label.ToLower().Contains(tag)) {
                        res.Add(photo);
                        break;
                    }
                }
            }
            return res;
        }
    }
}