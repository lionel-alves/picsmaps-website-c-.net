﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Security.Cryptography;

namespace BusinessManagement {
    public class Compte {


        public static void creerCompte(DataAccess.Membership cpt) {
            CompteAcces.CreerCompte(cpt);
        }

        public static void creerCompte(string login, string email, string nom, string prenom, string mdp, string urlPhotoProfil) {
            using (MD5 hasher = MD5.Create()) {
                CompteAcces.CreerCompte(new DataAccess.Membership() {
                    Login = login,
                    Email = email,
                    Nom = nom,
                    Prenom = prenom,
                    Mdp = mdp,
                    Photo_Profil = urlPhotoProfil
                });
            }
        }




        public static void UpdateCompte(DataAccess.Membership newCpt, string oldLogin) {
            if (!CompteAcces.UpdateCompte(newCpt, oldLogin)) {
                throw new BusinessManagementException("Update Failed");
            }
        }

        //public static void UpdateCompte(string login, string email, string nom, string prenom) {
        //    DataAccess.Membership cptToUpdate = CompteAcces.findUniqueCompte(login);

        //    if (!CompteAcces.UpdateCompte(new DataAccess.Membership() {
        //        Login = login,
        //        Email = email,
        //        Nom = nom,
        //        Prenom = prenom
        //    }, cptToUpdate)) {
        //        throw new BusinessManagementException("Update Failed");
        //    }
        //}

        public static DataAccess.Membership findByLogin(string login) {
            return CompteAcces.findUniqueCompte(login);
        }

        public static Membership findById(int idCpt)
        {
            return CompteAcces.findById(idCpt);
        }

        public static List<DataAccess.Membership> findByCriteria(int index, int offset, string login,
            string email, string nom, string prenom) {
                List<DataAccess.Membership> res =  CompteAcces.findCompteByCriteria(index, offset, login, email, nom, prenom);
                return res == null ? new List<DataAccess.Membership>() : res;
        }

        public static List<DataAccess.Membership> findByCriteria(int index, int offset, DataAccess.Membership cpt) {
            List<DataAccess.Membership> res = CompteAcces.findCompteByCriteria(index, offset, cpt.Login, cpt.Email, cpt.Nom, cpt.Prenom);
            return res == null ? new List<DataAccess.Membership>() : res;
        }

        public static void deleteCompte(String login) {
            if (!CompteAcces.deleteCompte(login)) {
                throw new BusinessManagementException("Failed to delete account with login " + login);
            }
        }

        public static void followCompte(string loginOrigin, string loginSuivi) {
            if (!CompteAcces.followCompte(loginOrigin, loginSuivi)) {
                throw new BusinessManagementException("failed to link account " + loginOrigin + " to account " + loginSuivi);
            }
        }

        public static void unFollowCompte(string loginOrigin, string loginSuivi) {
            try {
                if (!CompteAcces.unFollowCompte(loginOrigin, loginSuivi)) {
                    throw new BusinessManagementException("failed to unLink account " + loginOrigin + " to account " + loginSuivi);
                }
            } catch (Exception e) {
                
                throw new BusinessManagementException("Internal Server error", e);
            }
        }

        public static List<Membership> findByKey(int index, int offset, string key) {
            try {
                List<DataAccess.Membership> res = DataAccess.CompteAcces.findByKey(index, offset, key);
                return res == null ? new List<DataAccess.Membership>() : res;
            } catch (Exception e) {

                throw new BusinessManagementException("Internal Server error", e);
            }
        }

        public static List<Membership> findByKey(int index, int offset, string key, List<DataAccess.Membership> listToFilter)
        {
            try
            {
                List<DataAccess.Membership> res = DataAccess.CompteAcces.findByKey(index, offset, key, listToFilter);
                return res == null ? new List<DataAccess.Membership>() : res;
            }
            catch (Exception e)
            {

                throw new BusinessManagementException("Internal Server error", e);
            }
        }
    }
}
