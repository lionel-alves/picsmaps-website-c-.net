﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessManagement {
    public class Commentaire {
        public static void createComment(int photoId, string comment, string userName) {
            try {
                DataAccess.CommentaireAcces.createComment(userName, comment, photoId);
            } catch (Exception e) {    
                throw new BusinessManagementException("Internal servor error", e);
            }
        }

        public static void deleteComment(int commentId) {
            try {
                if (!DataAccess.CommentaireAcces.deleteComment(commentId)) {
                    throw new BusinessManagementException("Failed to delete comment with id " + commentId);
                }
            } catch (Exception e) {              
                throw new BusinessManagementException("Internal servor error", e);
            }
        }

        public static List<DataAccess.Commentaire> findComments(int offset, int index, int photoId) {
            try {
                List<DataAccess.Commentaire> res = DataAccess.CommentaireAcces.findComments(offset, index, photoId);
                return res == null ? new List<DataAccess.Commentaire>() : res;
            } catch (Exception e) {        
                throw new BusinessManagementException("Internal servor error", e);
            }
        }
    }
}
