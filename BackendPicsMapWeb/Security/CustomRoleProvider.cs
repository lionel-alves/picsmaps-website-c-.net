﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace BackendPicsMapWeb.Security {
    public class CustomRoleProvider : RoleProvider {

        public override void AddUsersToRoles(string[] usernames, string[] roleNames) {
            throw new NotImplementedException();
        }

        public override string ApplicationName {
            get {
                throw new NotImplementedException();
            }
            set {
                throw new NotImplementedException();
            }
        }

        public override void CreateRole(string roleName) {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole) {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch) {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles() {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username) {
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(username);

            List<String> res = new List<string>();
            foreach (DataAccess.Role role in cpt.Roles) {
                res.Add(role.value);
            }

            return res.ToArray();
        }

        public override string[] GetUsersInRole(string roleName) {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName) {
            DataAccess.Membership cpt  = BusinessManagement.Compte.findByLogin(username);

            foreach (DataAccess.Role role in cpt.Roles) {
                if (role.value.ToLower().Equals(roleName.ToLower())) {
                    return true;
                }
            }

            return false;
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames) {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName) {
            throw new NotImplementedException();
        }
    }
}