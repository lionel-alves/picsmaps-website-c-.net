﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess {
    public class PhotoAcces {
        public static List<Photo> findForAccount(int index, int offset, string login) {
            List<Photo> res = null;
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                res = (from p in bdd.Photos.Include("Commentaires")
                       where p.Membership.Login == login
                       orderby p.Id
                       select p).Skip(offset * index).Take(offset).ToList();
            }
            return res;
        }

        public static List<Photo> findFriendPictureForAccount(int index, int offset, string login) {
            List<Photo> res = new List<Photo>();
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                Membership cpt = (from c in bdd.Memberships
                                  where c.Login == login
                                  select c).FirstOrDefault();


                if (!cpt.CompteSuivi.IsLoaded) {
                    cpt.CompteSuivi.Load();
                }

                foreach (Membership cptSuivi in cpt.CompteSuivi) {
                    if (!cptSuivi.Photos.IsLoaded) {
                        cptSuivi.Photos.Load();
                        foreach (Photo pht in cptSuivi.Photos) {
                            if (!pht.Tags.IsLoaded) {
                                pht.Tags.Load();
                            }
                        }
                        }
                    foreach (Photo photo in cptSuivi.Photos.Skip(offset * index).Take(offset)) {
                        res.Add(photo);
                    }
                }
                return cpt.CompteSuivi.Count == 0 || cpt.CompteSuivi == null ?
                    null : (from r in res
                           orderby r.date_Poste
                           select r).ToList();
            }
        }

        public static List<Photo> findFriendPictureForAccount(int index, int offset, string login, DateTime dateLimit)
        {
            List<Photo> res = new List<Photo>();
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                Membership cpt = (from c in bdd.Memberships
                                  where c.Login == login
                                  select c).FirstOrDefault();


                if (!cpt.CompteSuivi.IsLoaded) {
                    cpt.CompteSuivi.Load();   
                }

                foreach (Membership cptSuivi in cpt.CompteSuivi) {
                    if (!cptSuivi.Photos.IsLoaded) {
                            cptSuivi.Photos.Load();
                            foreach (Photo pht in cptSuivi.Photos) {
                                if (!pht.Tags.IsLoaded) {
                                    pht.Tags.Load();
                                }
                            }
                        }
                    foreach (Photo photo in cptSuivi.Photos.Skip(offset * index).Take(offset)) {
                        if (photo.date_Poste.Value.CompareTo(dateLimit) >= 0) {
                            res.Add(photo);
                        }
                    }
                }
                return cpt.CompteSuivi.Count == 0 || cpt.CompteSuivi == null ?
                    null : (from r in res
                        orderby r.date_Poste
                        select r).ToList();
            }
        }

        public static void createPhoto(string login, List<DataAccess.Tag> tags, DataAccess.Photo photoToAdd) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                Membership compte = (from c in bdd.Memberships
                                     where c.Login == login
                                     select c).FirstOrDefault();

                photoToAdd.date_Poste = DateTime.Now;
                photoToAdd.Membership = compte;
                foreach (Tag tag in tags) {

                    photoToAdd.Tags.Add(tag);
                }

                bdd.AddToPhotos(photoToAdd);
                bdd.SaveChanges();
            }
        }

        public static List<Photo> findLastPhoto(int index, int offset, DateTime dateLimit) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                return (from p in bdd.Photos.Include("Commentaires").Include("Tags")
                        where p.date_Poste > dateLimit
                        orderby p.date_Poste
                        select p).Skip(offset * index).Take(offset).ToList();

            }
        }
        public static List<Photo> findLastPhoto(int index, int offset) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                return (from p in bdd.Photos.Include("Commentaires").Include("Tags")
                        orderby p.date_Poste
                        select p).Skip(offset * index).Take(offset).ToList();

            }
        }

        public static void updatePhoto(Photo photoToUpdate, Photo updatedPhoto) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                photoToUpdate.Titre = updatedPhoto.Titre == null ? photoToUpdate.Titre : updatedPhoto.Titre;
                photoToUpdate.Description = updatedPhoto.Description == null ? photoToUpdate.Description : updatedPhoto.Description;
                photoToUpdate.Url = updatedPhoto.Url == null ? photoToUpdate.Url : updatedPhoto.Url;
                bdd.SaveChanges();
            }
        }

        public static Photo findById(int id) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                return (from p in bdd.Photos.Include("Tags")
                        where p.Id == id
                        select p).FirstOrDefault();
            }
        }

        public static bool deleteTag(string tag, int idPhoto) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                Photo pht = (from p in bdd.Photos.Include("Tags")
                             where p.Id == idPhoto
                             select p).FirstOrDefault();
                foreach (Tag tagElt in pht.Tags) {
                    if (tag.ToLower().Equals(tagElt.Label.ToLower())) {
                        pht.Tags.Remove(tagElt);
                        bdd.SaveChanges();
                        return true;
                    }
                }
                return false;
            }
        }
        
        public static void addTag(string tag, int idPhoto) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                deleteTag(tag, idPhoto);
                Photo pht = (from p in bdd.Photos.Include("Tags")
                             where p.Id == idPhoto
                             select p).FirstOrDefault();
                pht.Tags.Add(new Tag() {
                    Photo = pht,
                    Label = tag.ToLower()
                });
                bdd.SaveChanges();
            }
        }

        public static List<Photo> getByTag(int index, int offset, string tag) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                List<Tag> tagGet = (from t in bdd.Tags.Include("Photo").Include("Tags")
                              where t.Label.ToLower() == tag.ToLower()
                              select t).Skip(index * offset).Take(offset).ToList();
                List<Photo> res = new List<Photo>();
                foreach (Tag singleTag in tagGet) {
                    res.Add(singleTag.Photo);
                }
                return res;
            }
        }
    }
}
