﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace DataAccess {
    public class CompteAcces {

        public static void CreerCompte(Membership cpt) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                using (MD5 md5 = MD5.Create()) {
                    cpt.Mdp = GetMd5Hash(md5, cpt.Mdp);
                }
                bdd.AddToMemberships(cpt);
                bdd.SaveChanges();
            }
        }

        public static string GetMd5Hash(MD5 md5Hash, string input) {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++) {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        /// <summary>
        /// Selection par login puis Update.
        /// Le login ne doit pas changer
        /// Attention ne change que les champs de la table pas les relations
        /// </summary>
        /// <param name="cpt"></param>
        public static bool UpdateCompte(Membership cpt, string login) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                Membership cptToUpdate = (from c in bdd.Memberships
                                          where c.Login == login
                                          select c).FirstOrDefault();

                    cptToUpdate.Nom = cpt.Nom;
                    cptToUpdate.Prenom = cpt.Prenom;
                    cptToUpdate.Email = cpt.Email;
                    cptToUpdate.Login = cptToUpdate.Login;
                    cptToUpdate.Mdp = cpt.Mdp;
                    cptToUpdate.Photo_Profil = cpt.Photo_Profil;
                    bdd.SaveChanges();
                    return true;

            }
        }

        public static Membership findUniqueCompte(String login) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                Membership res = (from c in bdd.Memberships.Include("CompteSuivi").Include("CompteSuiveur").Include("Photos").Include("Roles")
                              where c.Login == login
                              select c).FirstOrDefault();

                return res;
            }
        }

        public static Membership findById(int idCpt)
        {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities())
            {
                Membership res = (from c in bdd.Memberships.Include("CompteSuivi").Include("CompteSuiveur").Include("Roles").Include("Photos")
                                  where c.Id == idCpt
                                  select c).FirstOrDefault();

                return res;
            }
        }

        public static List<Membership> findCompteByCriteria(int index, int offset, string login,
            string email, string nom, string prenom) {
                List<Membership> res = null;
                using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                    List<Membership> test = (from c in bdd.Memberships
                                         where (!String.IsNullOrEmpty(login) && c.Login == login) ||
                                         (!String.IsNullOrEmpty(email) && c.Email == email) || 
                                         (!String.IsNullOrEmpty(nom) && c.Nom == nom) ||
                                         (!String.IsNullOrEmpty(nom) && c.Prenom == prenom)
                                         orderby c.Id
                                         select c).Skip(offset * index).Take(offset).ToList();
                    res = test;
                }
                return res;
        }

        public static bool deleteCompte(string login) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                Membership cptToDelete = (from c in bdd.Memberships
                                      where c.Login == login
                                      select c).FirstOrDefault();
                if (null != cptToDelete) {
                    bdd.Memberships.DeleteObject(cptToDelete);
                    bdd.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Utisation Origine suit suivi.
        /// </summary>
        /// <param name="loginOrigin"></param>
        /// <param name="loginSuivi"></param>
        /// <returns></returns>
        public static bool followCompte(string loginOrigin, string loginSuivi) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                Membership cptSuivi = (from c in bdd.Memberships
                                      where c.Login == loginSuivi
                                      select c).FirstOrDefault();
                Membership cptSuiveur = (from c in bdd.Memberships
                                      where c.Login == loginOrigin
                                     select c).FirstOrDefault();

                if (cptSuivi == null || cptSuiveur == null) {
                    return false;
                }
                if (!cptSuiveur.CompteSuivi.IsLoaded) {
                    cptSuiveur.CompteSuivi.Load();
                }
                cptSuiveur.CompteSuivi.Add(cptSuivi);

                bdd.SaveChanges();
                return true;
            }
        }

        public static bool unFollowCompte(string loginOrigin, string loginSuivi) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                Membership cpt = (from c in bdd.Memberships.Include("CompteSuivi")
                                       where c.Login == loginOrigin
                                       select c).FirstOrDefault();
                foreach (Membership cptSuivi in cpt.CompteSuivi) {
                    if (loginSuivi == cptSuivi.Login) {
                        cpt.CompteSuivi.Remove(cptSuivi);
                        bdd.SaveChanges();
                        return true;
                    }
                }
                return false;
            }
        }

        public static List<Membership> findByKey(int index, int offset, string key)
        {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities())
            {
                return (from c in bdd.Memberships.Include("CompteSuivi").Include("CompteSuiveur").Include("Photos")
                        where c.Login.Contains(key) ||
                        c.Nom.Contains(key) ||
                        c.Prenom.Contains(key) ||
                        c.Email.Contains(key)
                        orderby c.Id
                        select c).Skip(offset * index).Take(offset).ToList();
            }
        }
        
        public static List<Membership> findByKey(int index, int offset, string key, List<DataAccess.Membership> listToFilter) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
            return (from c in listToFilter
                              where c.Login.Contains(key) ||
                              c.Nom.Contains(key) ||
                              c.Prenom.Contains(key) ||
                              c.Email.Contains(key)
                              orderby c.Id
                              select c).Skip(offset * index).Take(offset).ToList();
            }
        }
    }
}
