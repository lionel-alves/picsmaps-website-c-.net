﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess {
    public class TagAcces {

        public static void createTag(string value, Photo photo) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                Tag newTag = new Tag() {
                    Label = value,
                    Photo = photo
                };
                bdd.AddToTags(newTag);
                bdd.SaveChanges();
            }
        }
    }
}
