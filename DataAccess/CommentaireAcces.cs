﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess {
    public class CommentaireAcces {
        public static void createComment(string login, string comment, long id) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                Membership cpt = (from c in bdd.Memberships
                                  where c.Login == login
                                  select c).FirstOrDefault(); ;

                Photo photo = (from p in bdd.Photos
                               where p.Id == id
                               select p).FirstOrDefault();
                Commentaire comm = new Commentaire() {
                    Date = DateTime.Now,
                    Photo = photo,
                    Texte = comment,
                    Membership = cpt
                };
                bdd.AddToCommentaires(comm);
                bdd.SaveChanges();
            }
        }

        public static bool deleteComment(int commentId) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                Commentaire commToDelete = (from c in bdd.Commentaires
                                            where c.Id == commentId
                                            select c).FirstOrDefault();
                if (null != commToDelete) {
                    bdd.Commentaires.DeleteObject(commToDelete);
                    bdd.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public static List<DataAccess.Commentaire> findComments(int offset, int index, int photoId) {
            List<DataAccess.Commentaire> res = new List<Commentaire>();

            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                res = (from c in bdd.Commentaires.Include("Photo").Include("Membership")
                        where c.Photo.Id == photoId
                        orderby c.Date descending
                        select c).Skip(offset * index).Take(offset).ToList();
            }

            return res;
        }
    }
}
