﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess {
    public class EvaluationAcces {
        public static void createEval(long photoId, string login, short note) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                Membership cpt = (from c in bdd.Memberships
                                  where c.Login == login
                                  select c).FirstOrDefault();

                Photo photo = (from p in bdd.Photos
                               where p.Id == photoId
                               select p).FirstOrDefault();
                
                Evaluation eval = new Evaluation() {
                    Photo = photo,
                    Membership = cpt,
                    Note = note
                };
                bdd.AddToEvaluations(eval);
                bdd.SaveChanges();
            }
        }

        public static void deleteEval(long photoId, string login) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                Evaluation eval = (from e in bdd.Evaluations.Include("Photo").Include("Membership")
                                   where e.Membership.Login == login &&
                                   e.Photo.Id == photoId
                                   select e).FirstOrDefault();

                bdd.Evaluations.DeleteObject(eval);
                bdd.SaveChanges();
            }
        }

        public static double findEvalsForPicture(long photoId) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                List<Evaluation> evals =  (from e in bdd.Evaluations.Include("Photo").Include("Membership")
                        where e.Photo.Id == photoId
                        orderby e.Note
                        select e).ToList();
                double res = 0;
                int nbEval = 0;
                foreach (Evaluation eval in evals) {
                    res += eval.Note;
                    nbEval++;
                }
                return res / nbEval;
            }
        }

        public static bool hasEvaluate(long photoId, string login) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                Evaluation eval = (from e in bdd.Evaluations.Include("Photo").Include("Membership")
                                   where e.Membership.Login == login &&
                                   e.Photo.Id == photoId
                                   select e).FirstOrDefault();

                return eval != null;
            }
        }
    }
}
